package net.cubekrowd.heycubebot;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayDeque;
import java.util.List;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;
import java.util.stream.Collectors;
import net.luckperms.api.LuckPermsProvider;
import net.luckperms.api.event.node.NodeMutateEvent;
import net.luckperms.api.model.data.DataType;
import net.luckperms.api.model.user.User;
import net.luckperms.api.node.NodeType;
import net.luckperms.api.node.types.InheritanceNode;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.event.EventHandler;

public class HeyCubeBotPlugin extends Plugin implements Listener {
    public static HeyCubeBotPlugin INSTANCE;
    public static final int BUF_SIZE = 1024;
    public final Object lock = new Object();
    public Queue<Task> tasks;
    public volatile boolean socketAlive;
    public Thread channelThread;

    @Override
    public void onEnable() {
        INSTANCE = this;
        tasks = new ArrayDeque<>();
        channelThread = new Thread(this::runChannel);
        channelThread.start();
        getProxy().getPluginManager().registerCommand(this, new DiscordCommand());
        getProxy().getPluginManager().registerListener(this, this);

        // Listen to LuckPerms events.
        var eventBus = LuckPermsProvider.get().getEventBus();
        eventBus.subscribe(NodeMutateEvent.class, this::onNodeMutate);
    }

    @Override
    public void onDisable() {
        channelThread.interrupt();
        while (channelThread.isAlive()) {
            try {
                channelThread.join();
            } catch (InterruptedException e) {
                // ignore interrupts
            }
        }

        tasks = null;
        INSTANCE = null;
    }

    public void onNodeMutate(NodeMutateEvent e) {
        // Only process normal mutation of users
        if (!e.isUser() || e.getDataType() != DataType.NORMAL) {
            return;
        }

        var user = (User) e.getTarget();

        // get uuid, name, and lowercase rank names
        var playerUUID = user.getUniqueId();
        var ranks = user.getNodes().stream()
            .filter(n -> n.getType() == NodeType.INHERITANCE)
            .map(n -> ((InheritanceNode) n).getGroupName())
            .map(String::toLowerCase)
            .collect(Collectors.toList());

        // FIXME: LuckPerms player names are all lowercase, so we can't use
        // this to update the discord! Leaving playername as empty string,
        // which in the protocol will mean "leave as-is"...
        var playerName = user.getUsername();

        // submit task
        this.pushTask(new UpdateDataTask(playerUUID, ranks, ""))
            .thenAccept(t -> {
                if (t.error != null) {
                    getLogger().warning("[from LP] Failed to update ranks for " + playerName + ": " + t.error);
                } else {
                    getLogger().info("[from LP] Successfully updated ranks for " + playerName);
                }
            }).exceptionally(exc -> {
                getLogger().warning("[from LP] Failed to update ranks for " + playerName);
                return null;
            });
    }

    @EventHandler
    public void onPostLogin(PostLoginEvent e) {
        var player = e.getPlayer();
        var playerName = player.getName();
        var playerUUID = player.getUniqueId();

        // push UpdateDataTask for updating the player
        this.getRanks(playerUUID).thenCompose(ranks -> {
            return this.pushTask(new UpdateDataTask(playerUUID, ranks, playerName));
            }).thenAccept(t -> {
                if (t.error != null) {
                    getLogger().warning("[from login] Failed to update ranks for " + playerName + ": " + t.error);
                } else {
                    getLogger().info("[from login] Successfully updated ranks for " + playerName);
                }
            }).exceptionally(exc -> {
                getLogger().warning("[from login] Failed to update ranks for " + playerName);
                return null;
            });
    }

    public CompletableFuture<List<String>> getRanks(UUID playerUuid) {
        var api = LuckPermsProvider.get();
        return api.getUserManager()
                .loadUser(playerUuid)
                .thenApply(u -> u.getNodes()
                        .stream()
                        .filter(n -> n.getType() == NodeType.INHERITANCE)
                        .map(n -> ((InheritanceNode) n).getGroupName())
                        .map(String::toLowerCase)
                        .collect(Collectors.toList()));
    }

    public <T extends Task> CompletableFuture<T> pushTask(T task) {
        var res = new CompletableFuture<T>();
        task.future = (CompletableFuture) res;
        synchronized (lock) {
            tasks.add(task);
            lock.notify();
        }
        return res;
    }

    public Socket openSocket() {
        for (;;) {
            try {
                var socket = new Socket("localhost", 26701);
                socket.setTcpNoDelay(true);
                socketAlive = true;
                return socket;
            } catch (IOException e) {
                // just ignore for now
            }

            try {
                // try to reconnect after some time
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                return null;
            }
        }
    }

    public void runChannel() {
        var socket = openSocket();

        mainLoop:
        for (;;) {
            Exception error = null;
            try {
                if (socket == null) {
                    break;
                }

                // Get a task to send to CubeBot
                Task task;
                synchronized (lock) {
                    for (;;) {
                        task = tasks.poll();
                        if (task != null) {
                            break;
                        }
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            // stop thread
                            break mainLoop;
                        }
                    }
                }

                if (socket.isClosed()) {
                    // we probably got disconnected... let's try to reconnect.
                    socket = openSocket();
                }

                // Send the task and receive a response.
                var msg = task.getMessage();
                String response = null;
                try {
                    // Get input and output streams for the socket
                    var writer = new PrintWriter(socket.getOutputStream(), true);
                    var reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                    // Send request to CubeBot server.
                    writer.println(msg);

                    // Receive response from CubeBot server. There should only be one.
                    response = reader.readLine();
                } catch (IOException e) {
                    error = e;
                }

                if (response == null) {
                    // something happened wrong in networking
                    error = new IOException("stream closed");
                } else {
                    task.processResponse(response);
                }

                if (error == null) {
                    task.future.complete(task);
                } else {
                    task.future.completeExceptionally(error);
                }

            } catch (Exception ee) {
                error = ee;
            }


            if (error != null) {
                // something bad happened, close connection and try to reconnect
                socketAlive = false;
                getLogger().warning("An error occurred, reconnecting to CubeBot. See stack trace:");
                error.printStackTrace();

                try {
                    socket.close();
                } catch (IOException e) {
                    getLogger().log(Level.WARNING, "Failed to close socket", e);
                }

                socket = openSocket();
            }
        }

        getLogger().warning("Network thread is closing!!!");

        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                // ignore
            }
        }
    }
}
