package net.cubekrowd.heycubebot;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.luckperms.api.LuckPermsProvider;

public class DiscordCommand extends Command {
    private final String PREFIX = ChatColor.DARK_GRAY + "[" +
            ChatColor.DARK_AQUA + "C" + ChatColor.GOLD + "K" +
            ChatColor.BLUE + "Discord" + ChatColor.DARK_GRAY + "] ";

    public DiscordCommand() {
        super("discord", "cubebot.linkup");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            sender.sendMessage("This command can only be run by a player.");
            return;
        }
        var p = (ProxiedPlayer) sender;
        var plugin = HeyCubeBotPlugin.INSTANCE;

        if (args.length == 0) {
            p.sendMessage(PREFIX + ChatColor.GRAY + "How do I link up to Discord?\n" +
                    PREFIX + ChatColor.GREEN + "1. Join " +
                    ChatColor.AQUA + "https://discord.cubekrowd.net " + ChatColor.GREEN + ".\n" +
                    PREFIX + ChatColor.GREEN + "2. Type " + ChatColor.AQUA + "!linkid " +
                    ChatColor.GREEN + "in " + ChatColor.BLUE + "#linkup " +
                    ChatColor.GREEN + "to get your linkup ID.\n" +
                    PREFIX + ChatColor.GREEN + "3. Type " +
                    ChatColor.AQUA + "/discord linkup <id> " + ChatColor.GREEN + "in CubeKrowd\n" +
                    PREFIX + ChatColor.GREEN + " " + ChatColor.GRAY + " " +
                    ChatColor.GREEN + " to get your " + ChatColor.ITALIC + "linkup code" +
                    ChatColor.GREEN + ".\n" +
                    PREFIX + ChatColor.GREEN + "4. Type your linkup code in the " +
                    ChatColor.BLUE + "#linkup " + ChatColor.GREEN + "chat.\n" +
                    PREFIX + ChatColor.GREEN + "That's it!");
        } else if (args[0].equalsIgnoreCase("linkup")) {

            final UUID playerUUID;
            final String playerName;
            final boolean force;

            if (args.length < 2 || args.length > 3) {
                // need to specify at least the discord username or member ID
                p.sendMessage(PREFIX + ChatColor.RED + "Please tell me who you are on Discord.\n" +
                        PREFIX + ChatColor.RED + "Usage: " +
                        ChatColor.AQUA + "/discord linkup <discordid>\n" +
                        PREFIX + ChatColor.GREEN + "where <discordid> is either your username#0000,\n" +
                        PREFIX + ChatColor.GREEN + "or a member ID number obtained by typing " +
                        ChatColor.AQUA + "!linkid\n" +
                        PREFIX + ChatColor.GREEN + "in the " + ChatColor.BLUE + "#linkup " +
                        ChatColor.GREEN + "chat.");
                return;
            } else if (args.length == 3) {
                // in this case we want to force the linkup. We expect the
                // fourth argument to be the target player name.
                // This method relies on the LuckPerms UUID cache, and may
                // be slow.

                if (!sender.hasPermission("cubebot.linkup.others")) {
                    p.sendMessage(PREFIX + ChatColor.RED + "No permission: try /discord linkup <discordid>.");
                    return;
                }

                var api = LuckPermsProvider.get();
                playerName = args[2];
                try {
                    playerUUID = api.getUserManager().lookupUniqueId(playerName).get();
                } catch (Exception e) {
                    p.sendMessage(PREFIX + ChatColor.RED + "Failed to find uuid for username");
                    return;
                }
                force = true;
            } else {
                playerUUID = p.getUniqueId();
                playerName = p.getName();
                force = false;
            }

            // this could be a username#0000 or a member ID. We assume that
            // CubeBot will sort that out.
            var discordId = args[1];

            plugin.getRanks(playerUUID).thenCompose(ranks -> {
                return plugin.pushTask(new LinkupTask(playerUUID, ranks, playerName, discordId, force));
            }).thenAccept(t -> {
                if (t.error != null) {
                    p.sendMessage(PREFIX + ChatColor.RED + "Error: " + t.error);
                    plugin.getLogger().warning("[from command] Failed to link up " + playerName + ": " + t.error);
                } else if (t.linkupCode == null && !force) {
                    // already linked up, not force
                    p.sendMessage(PREFIX + ChatColor.RED + "You have already linked your in-game account to Discord!\n" +
                            PREFIX + ChatColor.GRAY + "You can do " + ChatColor.AQUA + "/discord unlink" + ChatColor.GRAY + " to unlink your account.");
                    plugin.getLogger().info("[from command] Tried to linkup " + playerName + ", but they were linked");
                } else if (force) {
                    // forcing link up or update
                    p.sendMessage(PREFIX + ChatColor.GREEN + "Successfully linked accounts.");
                    plugin.getLogger().info("[from command] Force-linked " + playerName);
                } else {
                    // not yet linked up, getting code
                    var code = t.linkupCode;
                    p.sendMessage("\n" +
                            PREFIX + ChatColor.GREEN + "Your code is: " + ChatColor.GOLD + code + "\n" +
                            PREFIX + ChatColor.GREEN + "Now type " + ChatColor.AQUA + code + ChatColor.GREEN + " in the " + ChatColor.BLUE + "#linkup" + ChatColor.GREEN + " chat!" +
                            "\n" +
                            PREFIX + ChatColor.GREEN + "Link: " + ChatColor.GRAY + "https://discord.cubekrowd.net/" +
                            "\n");
                    plugin.getLogger().info("[from command] Allocated linkup code for " + playerName);
                }
            }).exceptionally(e -> {
                p.sendMessage(PREFIX + ChatColor.RED + "Something went wrong. Please try again later!");
                plugin.getLogger().warning("[from command] Failed to link up " + playerName);
                return null;
            });
        } else if (args[0].equalsIgnoreCase("unlink")) {
            if (!sender.hasPermission("cubebot.unlink")) {
                p.sendMessage(PREFIX + ChatColor.RED + "No permission");
                return;
            }
            final UUID playerUUID;
            final String playerName;

            if (args.length < 1 || args.length > 2) {
                p.sendMessage(PREFIX + ChatColor.RED + "Usage: /discord unlink");
                return;
            } else if (args.length == 2) {
                // in this case we want to force the linkup. We expect the
                // fourth argument to be the target player name.
                // This method relies on the LuckPerms UUID cache, and may
                // be slow.

                if (!sender.hasPermission("cubebot.unlink.others")) {
                    p.sendMessage(PREFIX + ChatColor.RED + "No permission: try /discord unlink."); 
                    return;
                }

                var api = LuckPermsProvider.get();
                playerName = args[1];
                try {
                    playerUUID = api.getUserManager().lookupUniqueId(playerName).get();
                } catch (Exception e) {
                    p.sendMessage(PREFIX + ChatColor.RED + "Failed to find uuid for username");
                    return;
                }
            } else {
                playerUUID = p.getUniqueId();
                playerName = p.getName();
            }

            plugin.pushTask(new UnlinkTask(playerUUID)).thenAccept(t -> {
                if (t.error != null) {
                    p.sendMessage(PREFIX + ChatColor.RED + "Error: " + t.error);
                    plugin.getLogger().warning("[from command] Failed to unlink " + playerName + ": " + t.error);
                } else if (t.unlinked) {
                    p.sendMessage(PREFIX + ChatColor.GREEN + "Your account is no longer linked to Discord. Please do " + ChatColor.AQUA + "/discord" + ChatColor.GREEN + " to connect again.");
                    plugin.getLogger().info("[from command] Unlinked " + playerName);
                } else {
                    p.sendMessage(PREFIX + ChatColor.RED + "You are not linked to any Discord account!" +
                            PREFIX + ChatColor.GRAY + "You can do " + ChatColor.AQUA + "/discord link" + ChatColor.GRAY + " to link your account.");
                    plugin.getLogger().info("[from command] Tried to unlink " + playerName + ", but they were already unlinked");
                }
            }).exceptionally(e -> {
                p.sendMessage(PREFIX + ChatColor.RED + "Something went wrong. Please try again later!");
                plugin.getLogger().warning("[from command] Failed to unlink " + playerName);
                return null;
            });
        } else if (args[0].equalsIgnoreCase("update")) {
            if (!sender.hasPermission("cubebot.update")) {
                p.sendMessage(PREFIX + ChatColor.RED + "No permission");
                return;
            }

            final UUID playerUUID;
            final String playerName;

            if (args.length < 1 || args.length > 2) {
                p.sendMessage(PREFIX + ChatColor.RED + "Usage: /discord update");
                return;
            } else if (args.length == 2) {
                // in this case we want to force the linkup. We expect the
                // fourth argument to be the target player name.
                // This method relies on the LuckPerms UUID cache, and may
                // be slow.

                if (!sender.hasPermission("cubebot.update.others")) {
                    p.sendMessage(PREFIX + ChatColor.RED + "No permission: try /discord update."); 
                    return;
                }

                var api = LuckPermsProvider.get();
                playerName = args[1];
                try {
                    playerUUID = api.getUserManager().lookupUniqueId(playerName).get();
                } catch (Exception e) {
                    p.sendMessage(PREFIX + ChatColor.RED + "Failed to find uuid for username");
                    return;
                }
            } else {
                playerUUID = p.getUniqueId();
                playerName = p.getName();
            }
            plugin.getRanks(playerUUID).thenCompose(ranks -> {
                return plugin.pushTask(new UpdateDataTask(playerUUID, ranks, playerName));
            }).thenAccept(t -> {
                if (t.error != null) {
                    p.sendMessage(PREFIX + ChatColor.RED + "Error: " + t.error);
                    plugin.getLogger().warning("[from command] Failed to update linkup for " + playerName + ": " + t.error);
                } else {
                    p.sendMessage(PREFIX + ChatColor.GREEN + "Successfully updated you on Discord!");
                    plugin.getLogger().info("[from command] Updated linkup for " + playerName);
                }
            }).exceptionally(e -> {
                p.sendMessage(PREFIX + ChatColor.RED + "Something went wrong. Please try again later!");
                plugin.getLogger().warning("[from command] Failed to update linkup for " + playerName);
                return null;
            });
        } else {
            p.sendMessage(PREFIX + ChatColor.RED + "Unknown command: /discord " + String.join(" ", args));
        }
    }
}
