package net.cubekrowd.heycubebot;

import java.util.UUID;

public class UnlinkTask extends Task {
    // request
    public UUID playerUuid;

    // response
    public boolean unlinked;

    public UnlinkTask(UUID playerUuid) {
        this.playerUuid = playerUuid;
    }

    @Override
    public String getMessage() {
        return "UNLINK:" + playerUuid;
    }

    @Override
    public void processResponse(String response) {
        this.error = null;
        String[] spl = response.split(":");
        if (spl.length != 2) {
            // invalid response
            this.error = "too many fields";
            return;
        }

        switch (spl[0]) {
            case "UNLINKED":
                // don't give the user the discord ID since it may be ugly :(
                this.unlinked = true;
                break;
            case "ERROR":
                this.error = decodeError(spl[1]);
                this.unlinked = false;
                break;
            default:
                this.error = "unknown response";
                this.unlinked = false;
                break;
        }
    }
}
