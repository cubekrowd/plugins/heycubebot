package net.cubekrowd.heycubebot;

import java.util.List;
import java.util.UUID;

public class UpdateDataTask extends Task {
    public UUID playerUuid;
    public List<String> ranks;
    public String playerName;

    public UpdateDataTask(UUID playerUuid, List<String> ranks, String playerName) {
        this.playerUuid = playerUuid;
        this.ranks = ranks;
        this.playerName = playerName;
    }

    @Override
    public String getMessage() {
        var msg = "DATA:" + playerUuid +
                "|" + String.join("&", ranks) +
                "|" + playerName;
        return msg;
    }

    @Override
    public void processResponse(String response) {
        this.error = null;

        if (response.equals("OK")) {
            return;
        }

        String[] spl = response.split(":");
        if (spl.length != 2) {
            // invalid response
            this.error = "too many fields";
            return;
        }

        switch (spl[0]) {
            case "ERROR":
                this.error = decodeError(spl[1]);
                break;
            default:
                this.error = "unknown response";
                break;
        }
    }
}
