package net.cubekrowd.heycubebot;

import java.util.List;
import java.util.UUID;

public class LinkupTask extends Task {
    // request
    public UUID playerUuid;
    public List<String> ranks;
    public String playerName;
    public boolean force;

    // response
    public String linkupCode;
    public String discordId;

    public LinkupTask(UUID playerUuid, List<String> ranks, String playerName, String discordId, boolean force) {
        this.playerUuid = playerUuid;
        this.ranks = ranks;
        this.playerName = playerName;
        this.discordId = b64Encode(discordId);
        this.force = force;
    }

    @Override
    public String getMessage() {
        var msg = "LINK:" + playerUuid +
                "|" + String.join("&", ranks) +
                "|" + playerName +
                "|" + discordId;
        if (force) {
            return "FORCE" + msg;
        } else {
            return msg;
        }
    }

    @Override
    public void processResponse(String response) {
        this.error = null;
        this.linkupCode = null;
        String[] spl = response.split(":");
        if (spl.length != 2) {
            // invalid response
            this.error = "too many fields";
            return;
        }

        switch (spl[0]) {
            case "LINKED":
                // don't set the discord ID here since it may be ugly :(
                break;
            case "CODE":
                this.linkupCode = spl[1];
                break;
            case "ERROR":
                this.error = decodeError(spl[1]);
                break;
            default:
                this.error = "unknown response";
                break;
        }

    }
}
