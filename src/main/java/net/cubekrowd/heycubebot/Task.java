package net.cubekrowd.heycubebot;

import static java.util.Map.entry;

import java.util.Base64;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.nio.charset.StandardCharsets;

public abstract class Task {

    private static final Base64.Decoder DECODER = Base64.getUrlDecoder();
    private static final Base64.Encoder ENCODER = Base64.getUrlEncoder();
    private static final Map<String, String> errorMap = Map.ofEntries(
            entry("ARGS", "invalid arguments in internal TCP connection"),
            entry("UNKNOWN", "CubeBot internal error"),
            entry("NOTLINKED", "not yet linked"),
            entry("UUID", "invalid minecraft UUID format"),
            entry("RANK", "undefined minecraft rank -> discord role mapping"),
            entry("UNKNOWN_UUID", "not yet in CubeBot's database"),
            entry("ALREADY_LINKED", "discord user already linked"),
            entry("INCORRECT_USER", "discord user not found. Make sure you are in the CubeKrowd discord server, check your spelling, or try typing !linkid in the #linkup channel and use the member ID number instead.")
    );

    public CompletableFuture future;
    public String error;

    public abstract String getMessage();
    public abstract void processResponse(String response);

    protected String b64Decode(String x) {
        return new String(DECODER.decode(x), StandardCharsets.UTF_8);
    }

    protected String b64Encode(String x) {
        return ENCODER.encodeToString(x.getBytes(StandardCharsets.UTF_8));
    }

    protected String decodeError(String err) {
        return errorMap.getOrDefault(err, "unknown error");
    }

}
