# Hey, CubeBot!

A BungeeCord client for
[CubeBot2](https://gitlab.com/cubekrowd/system/cubebot2).

We send messages via TCP to CubeBot to notify the discord bot of
linkup/update/unlink requests, rank changes, username changes,
and other Minecraft events.

This plugin replaces the `plugin` portion of
[CubeBot](https://gitlab.com/cubekrowd/system/cubebot).

## Commands and Permissions

- **/discord**: Manage linkups. Requires `cubebot.linkup` permission
  to see the help text.
- **/discord linkup \<discordid\> [minecraft_username]**: Link up accounts.
  - If `minecraft_username` is not specified, request a linkup code from
    CubeBot for the given `discordid` and the current Minecraft
    account. The `discordid` is interpreted as either
    `username#0000` discord tag or a discord member ID.
    Requires `cubebot.linkup` permission.
  - If `minecraft_username` is specified, directly link up the specified
    Minecraft account with the specified Discord member.
    Requires `cubebot.linkup.others` permission.
- **/discord unlink [minecraft_username]**: Unlink accounts.
  - If `minecraft_username` is not specified, unlink the current
    account from Discord. Requires `cubebot.unlink` permission.
  - If `minecraft_username` is specified, unlink that account.
    Requires `cubebot.unlink.others` permission.
- **/discord update [minecraft_username]**: Update linkups. This
  is provided to users in case their username or ranks become outdated
  and they wish for changes to propagate immediately. Because this
  plugin automatically pushes updates to usernames or ranks by listening
  to both BungeeCord and LuckPerms events, there should be no need to
  execute this command. However, in the event of a network failure or
  other reason for inconsistency between Minecraft and Discord, a user
  may need to run this command.
  - If `minecraft_username` is not specified, update the currently linked
    Discord account. Requires `cubebot.update` permission.
  - If `minecraft_username` is specified, update that account.
    Requires `cubebot.update.others` permission.

## Structure

HeyCubeBot uses a TCP connection to CubeBot to push commands and events
to Discord. When a command or event needs interaction to or from Discord,
it pushes a `Task` into a task queue and receives a `CompletableFuture<Task>`.
A network thread polls the task queue for tasks (using wait/notify), and
upon receiving one, sends it to CubeBot and receives a response. The network
thread then updates the task with the response and any errors, and continues
polling for more tasks.

It should be possible to add more Tasks (even in another plugin) by extending
`Task` and submitting these tasks to the task queue.
